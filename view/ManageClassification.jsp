<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Manage Classification Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <script type = "text/javascript">
      function togglediv(id) {
        var div = document.getElementById(id);
        div.style.display = div.style.display == "none" ? "block" : "none";
      }
    </script>
    <h1>For Admin: Add, Edit or Delete a Classification</h1>
    <form action="http://localhost:8080/zomato/user" method="post">
      <table style="with: 5%" align="right">
        <tr>
          <td>
            <input type="submit" value="logout" name="submit" />
          </td>
        </tr>
      </table>
    </form>
    <div class="ex">
      <form action="http://localhost:8080/zomato/restaurant" method="get">
        <table style="with: 50%">
          <h2>Please Click a button to do any operation:</h2>
            <tr>
              <input id="addclassification" type="button" 
                     value="add new classification"
                     onclick="togglediv('mynewclassificationdiv')">
              <div id="mynewclassificationdiv" style="display: none;">
                      Classification Name:
                    <input type="text" value="new classification name" 
                           id="classification name" name="classification name">
                    </input>
                <input type="submit" value="confirm add classification"/>
               <input type="hidden" value="get categories" name="submit" />
               <input type="hidden" value="view/ManageCategories.jsp" 
                      name="targetPage" />
                <br>
              </div>                        
            </tr>
            <br>
            <tr>
              <th>Classification Name</th>            
            </tr>
             <br>
             <c:forEach var="classification" items="${categories}">
             <tr>
               <th>
                 <c:out value="${classification.name}">
                 </c:out>
               </th>
               <th> 
                 <c:out>
                 <input id="deleteclassification" type="button" 
                        value="delete classification" 
                        onclick="togglediv('mydeleteclassificationdiv')">
                 <div id="mydeleteclassificationdiv" style="display: none;">
                   <input type="submit" value="confirm delete classification" />
               <input type="hidden" value="get categories" name="submit" />
               <input type="hidden" value="view/ManageCategories.jsp" 
                      name="targetPage" />
                   <br>
                   </div>
                 </c: out> 
                 </th>
               </tr>
             </c:forEach>
           </table>
         </form>
      </div>
  </body>
</html>
