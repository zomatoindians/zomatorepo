<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="d" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <h1>Restaurant Order Page</h1>
    <form action="http://localhost:8080/zomato/user" method="post">
      <table style="with: 5%" align="right">
        <tr>
          <td>
            <input type="submit" value="logout" name="submit" />
          </td>
        </tr>
      </table>
    </form>
    <div class="ex">
      <form action="http://localhost:8080/zomato/menuItem" method="get">
        <table style="with: 50%">
          <caption><h2>Restaurant Details</h2></caption>
            <table style="with: 20%">
              <tr>
                <th>
                  <c:set var="Name" value="${restaurant}"/> ${restaurant.name}
                </th>
                <th>
                  <c:set var="Description" value="${restaurant.description}"/>
                    ${restaurant.description}
                </th>
                <th>
                  <c:set var="startingTime" value="${restaurant.startingTime}"/>
                    ${restaurant.startingTime}
                </th>
                <th>
                  <c:set var="startingTime" value="${restaurant.closingTime}"/>
                    ${restaurant.closingTime}
                </th>
              </tr>
            </table>
            <caption>
              <h3>List of Options Available Options at ${restaurant.name}</h3>
            </caption>
            <tr>
              <th>Food Catogeries</th>
              <th>View Food Options</th>
            </tr>
            <br>
              <d:forEach var="category" items="${categories}">
            <tr>
              <td><d:out value="${category.name}" /></td>
              <td>
                <a href="http://localhost:8080/zomato/menuItem
                         ?id=${category.id}&submit=display menu items"> 
                  View 
                  <d:out value="${category.name}" />
                </a> 
              </td>
              <br>
            </tr>
            </d:forEach>
         </table>
       </form>
     </div>
  </body>
</html>
