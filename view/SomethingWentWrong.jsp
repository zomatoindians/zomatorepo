<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Welcome Page</title>
  </head>
  <style>
    div.ex {
      text-align: center width:300px;
      padding: 20px;
      border: 0px white;
      margin: 5px
    }
  </style>
  <body bgcolor = "sky blue">
    <marquee direction="right"><h1>Aw! Something is broken!</h1></marquee>
    <div class="ex">
      <center>
      <img border="0" alt="Bad robot!" src="media/badrobot.jpg" width="500" 
           height="210">
      <br>
      <h2>We encountered an error while loading your request!</h2>
      <h3> Relax! Our software engineers are working to fix this! <h3>
      <img border="0" alt="Worried Robot" src="media/badrobotconfused.jpg" 
           width="150" height="150">
      <img border="0" alt="Realxed Robot" src="media/badrobotchilling.jpg" 
           width="150" height="150">
      <br>  
      <i>You can go back <a href = "Welcome.jsp">here</a> and start again.</i> 
      <br>
      </center>      
    </div>
  </body>
</html>
