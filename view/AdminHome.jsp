<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Admin Home Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <h1>Admin Home Page</h1>
    <form action="http://localhost:8080/zomato/user" method="post">
      <table style="with: 5%" align="right">
        <tr>
          <td>
            <input type="submit" value="logout" name="submit" />
          </td>
        </tr>
      </table>
    </form>
    <div id="top"> Hello! ${user.firstName}, Welcome to Admin Console! </div>
    <div class="ex">
      <caption><h2>Please choose an Action to perform:</h2></caption>
      <table style="with: 50%">
        <th>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/user?submit=give
                     users&targetPage=view/ManageAdminAccess.jsp"> 
            <input type="button" value="give admin access" >
            </a>
          </td>  
        </tr>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/menuItem?submit=get
                     foods&targetPage=view/ManageFood.jsp"> 
            <input type="button" value="manage food" >
            </a>
          </td>  
        </tr>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/role?submit=get 
                     roles&targetPage=view/ManageRoles.jsp"> 
            <input type="button" value="manage role" >
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/discount?submit=get 
                     discounts&targetPage=view/ManageDiscount.jsp"> 
            <input type="button" value="manage discount" >
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/coupon?submit=get 
                     coupons&targetPage=view/ManageCoupon.jsp"> 
            <input type="button" value="manage coupon" >
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/restaurant?submit=get 
                     classifications&targetPage=view/ManageClassification.jsp"> 
            <input type="button" value="manage classification" >
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/payment?submit=get payment 
                     options&targetPage=view/ManagePaymentOption.jsp"> 
            <input type="button" value="manage payment option" >
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="http://localhost:8080/zomato/menuItem?submit=display all
                     categories&targetPage=view/ManageCategory.jsp"> 
            <input type="button" value="manage category" >
            </a>
          </td>
        </tr>
        </th>
      </table>
    </div>
  </body>
</html>
