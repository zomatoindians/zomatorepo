<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <script type = "text/javascript">
      function validateEmail(sEmail) {
      var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
        if(!sEmail.match(reEmail)) {
          alert("Please Enter a valid email address!");
          return false;
        }
        return true;
      }
    </script>
  </head>
  <body bgcolor = "#DEB887">
    <form method="post" action="http://localhost:8080/zomato/user" >
    <table border="1" width="30%" cellpadding="3">
     <thead>
       <tr>
         <th colspan="2">
           <center>Welcome! Please Login Here! </center>
         </th>
       </tr>
     </thead>
     <tbody>
       <tr>
         <td>Email ID</td>
         <td>
           <input type="email" name="email" onblur="validateEmail(this.value);" 
                  placeholder="Enter a valid email address" required/>
         </td>
       </tr>
       <tr>
         <td>Password</td>
         <td><input type="password" name="password"
                    placeholder="Enter Password" required/>
         </td>
       </tr>
       <tr>
         <td><input type="reset" value="Reset" /></td>
         <td><input type="submit" value="login" name="submit" /></td>
       </tr>
       <tr>
         <td>Forgot Password?</td>
         <td><a href="ForgotPassword.jsp"> Click Here <a/></td>
       </tr>
     </tbody>
   </table>
     <br>
     <img src="media/online-food-ordering.png" width="1250" height="500">
     </form>
  </body>
</html>
