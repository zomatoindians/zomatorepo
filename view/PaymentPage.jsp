<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Welcome Page</title>
    </head>
    <style>
        div.ex {
    	text-align: center width:300px;
    	padding: 20px;
    	border: 5px solid blue;
    	margin: 5px
    }
    </style>
    <body bgcolor = "sky blue">
    	<h1>Welcome!</h1>
    	<div class="ex">
    			<table style="with: 50%">
    			<h3> You can do following actions: </h3>
    				<tr>
    					<td><a href = "view/LoginUser.jsp"> Login </a></td>
    				</tr>
    				<tr>
    					<td><a href = "view/RegisterUser.jsp"> Register </a></td>
    				</tr>
    				<tr>
    					<td><a href = "view/ClassificationList.jsp"> Check out Restaurants </a></td>
    				</tr>
    			</table>
    			<br>
    			You can add reviews or order food online only after logging in!
    	</div>
    </body>
</html>s
