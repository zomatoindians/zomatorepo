<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <h1>Restaurant List</h1>
    <form action="http://localhost:8080/zomato/user" method="post">
      <table style="with: 5%" align="right">
        <tr>
          <td>
            <input type="submit" value="logout" name="submit" />
          </td>
        </tr>
      </table>
    </form>
    <div class="ex">
      <form action="http://localhost:8080/zomato/menuItem" method="get">
        <table style="with: 50%">
          <caption><h2>List of Restaurants</h2></caption>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Starting time</th>
            <th>Closing time</th>
            <th>Home Delivery</th>
            <th>Order food</th>
          </tr>
            <c:forEach var="restaurant" items="${restaurants}">
          <tr>
            <td><c:out value="${restaurant.name}" /></td>
            <td><c:out value="${restaurant.description}" /></td>
            <td><c:out value="${restaurant.startingTime}" /></td>
            <td><c:out value="${restaurant.closingTime}" /></td>
            <td><c:out value="${restaurant.homeDelivery}" /></td>
            <td>
              <a href="http://localhost:8080/zomato/menuItem
                       ?id=${restaurant.id}&submit=display categories
                       &targetPage=view/RestaurantList.jsp"> 
                View
                <c:out value="${restaurant.name}" />
              </a> 
            </td>
          </tr>
            </c:forEach>
        </table>
      </form>
    </div>
  </body>
</html>
