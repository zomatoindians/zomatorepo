<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Add New Address Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <h1>New Address Addition Form</h1>
     <div class="ex">
     <form action="http://localhost:8080/zomato/user" method="post">
       <table style="with: 60%">
         <tr>
           <td>Address Line 1 *</td>
           <td><input type="text" name="addressLineOne"
                      placeholder="Please Enter a value for this field" 
                      required/>
           </td>
         </tr>
         <tr>
           <td>Address Line 2</td>
           <td><input type="text" name="addressLineTwo" 
                      placeholder="This field is optional" />
           </td>
         </tr>
         <tr>
           <td>City *</td>
           <td><input type="text" name="city"
                      placeholder="Please Enter a value for this field" 
                      required/>
           </td>
         </tr>
         <tr>
           <td>State *</td>
           <td><input type="text" name="state"
                      placeholder="Please Enter a value for this field"  
                      required/>
           </td>
         </tr>
         <tr>         
           <td>Pin Code *</td>
           <td><input type="integer" name="pincode" 
                      placeholder="Please Enter a value for this field" 
                      required/>
           </td>
         </tr>
       </table>
       <br>
       <i>Fields marked with * are required fields</i>
       <br>
        <input type="reset" value="Reset" />
        <input type="submit" value="register" name="submit" />
      </form>
    </div>
  </body>
</html>
