<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Manage Food Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <script type = "text/javascript">
      function togglediv(id) {
        var div = document.getElementById(id);
        div.style.display = div.style.display == "none" ? "block" : "none";
      }
    </script>
    <h1>For Admin: Add, Edit or Delete a food</h1>
    <form action="http://localhost:8080/zomato/user" method="post">
      <table style="with: 5%" align="right">
        <tr>
          <td>
            <input type="submit" value="logout" name="submit" />
          </td>
        </tr>
      </table>
    </form>
    <div class="ex">
      <form action="http://localhost:8080/zomato/menuItem" method="get">
        <table style="with: 50%">
          <h2>Please Click a button to do any operation:</h2>
            <tr>
              <input id="addfood" type="button" value="add new food"
                     onclick="togglediv('mynewfooddiv')">
              <div id="mynewfooddiv" style="display: none;">
                      Food Name:
                    <input type="text" value="new food name" 
                           id="food name" name="food name">
                    </input>
                <input type="submit" value="confirm add food"
                       name="submit" />
               <input type="hidden" value="get categories" name="submit" />
               <input type="hidden" value="view/ManageCategories.jsp" 
                      name="targetPage" />
                <br>
              </div>                        
            </tr>
            <br>
            <tr>
              <th>Food Name</th>            
            </tr>
             <br>
             <c:forEach var="food" items="${categories}">
             <tr>
               <td>
                 <c:out value="${food.name}" />
               </td>
               <td> 
                 <input id="deletefood" type="button" 
                        value="delete food" 
                        onclick="togglediv('mydeletefooddiv')">
                 <div id="mydeletefooddiv" style="display: none;">
                   <input type="submit" value="confirm delete food" 
                          name="submit" />
               <input type="hidden" value="get categories" name="submit" />
               <input type="hidden" value="view/ManageCategories.jsp" 
                      name="targetPage" />
                   <br>
                   </div> 
                 </td>
               </tr>
             </c:forEach>
           </table>
         </form>
      </div>
  </body>
</html>
