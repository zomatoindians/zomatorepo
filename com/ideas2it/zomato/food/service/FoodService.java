package com.ideas2it.zomato.food.service;

import java.util.Set;

import com.ideas2it.zomato.food.Food;

/**
 * Interface implementation for food service class.
 */
public interface FoodService {

    /**
     * Adds a new food.
     *
     * @param food
     *            - food object about to be added .
     */
    void add(Food food);
    
    /**
     * Removes the food specified by id.
     *
     * @param id
     *            - id of food .
     */
    void remove(int id);
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database.
     */
    Set<Food> getFoods();
}
