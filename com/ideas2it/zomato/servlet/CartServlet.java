package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cart.controller.CartController;
import com.ideas2it.zomato.common.Constant;

/**
 * Servlet implementation for cart and cart details.
 */
public class CartServlet extends HttpServlet {

    CartController cartController = new CartController();

    private static final String ADD_TO_CART = "add to cart";
    private static final String DISPLAY_CART = "display cart";
    private static final String REMOVE_ITEM = "remove item";

    /**
     * Inserts and updates tables into the database for cart and cart details.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();

        switch (input) {
            case ADD_TO_CART :
                addToCart(request, response, session);
                break;
            case DISPLAY_CART :
                displayCart(request, response, session);
                break;
            case REMOVE_ITEM :
                removeItem(request, response, session);
                break;
        }
    }

    /**
     * Adds the menuItem to the cart.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *        - session object of HTTPservlet.
     */
    private void addToCart(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        int menuItemId = (Integer)(request.getAttribute("id"));
        int userId = (Integer)(session.getAttribute("userId"));
        cartController.addToCart(menuItemId, userId);
    }
    
    /**
     * Displays the user's cart.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *        - session object of HTTPservlet.
     */
    private void displayCart(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        int userId = (Integer)(session.getAttribute("userId"));
        Cart cart = cartController.getCart(userId);
        session.setAttribute("cart", cart);
    }
    
    /**
     * Removes the cart detail specified, from the cart.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *        - session object of HTTPservlet.
     */
    private void removeItem(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        int cartDetailId = (Integer)(session.getAttribute("cartDetailId"));
        cartController.removeCartDetail(cartDetailId);
    }
}
