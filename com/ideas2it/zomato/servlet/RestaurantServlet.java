package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.review.Review;
import com.ideas2it.zomato.restaurant.controller.RestaurantController;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.common.Constant;

/**
 * Servlet implementation for restaurant.
 */
public class RestaurantServlet extends HttpServlet {

    RestaurantController restaurantController = new RestaurantController();

    private static final String REGISTER = "register";
    private static final String ADD_CLASSIFICATION = "add classification";
    private static final String REMOVE_CLASSIFICATION = "remove classification";
    private static final String ASSIGN_CLASSIFICATION = "assign classification";
    private static final String ASSIGN_ADDRESS = "assign address";
    private static final String GET_RESTAURANTS = "get restaurants";
    private static final String GET_CLASSIFICATIONS = "get classifications";
    private static final String ADD_REVIEW = "add review";
    
    /**
     * Inserts and updates tables into the database for restaurant 
     * and classification.
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
			               throws ServletException, IOException {
	    response.setContentType("text/html");
	    String input = request.getParameter("submit");
	    HttpSession session = request.getSession();
	    switch (input) {
            case REGISTER :
                register(request, response, session);
                break;
            case ADD_CLASSIFICATION :
                addClassification(request, response);
                break;
            case REMOVE_CLASSIFICATION :
                removeClassification(request, response);
                break;
            case ASSIGN_CLASSIFICATION :
                assignClassification(request, response, session);
                break;
            case ADD_REVIEW :
                addReview(request, response, session);
                break;
        }
    }
    
    /**
     * Displays restaurant and classification related details.
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
                      throws ServletException, IOException {
        response.setContentType("text/html");
	    String input = request.getParameter("submit");
	    HttpSession session = request.getSession();
        
        switch(input) {
            case GET_CLASSIFICATIONS :
                getClassifications(request, response, session);
                break;
            case GET_RESTAURANTS :
                getRestaurants(request, response, session);
                break;
        }
    } 
    
    /**
     * Register a new restaurant.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void register(HttpServletRequest request, 
                          HttpServletResponse response, HttpSession session) 
                          throws ServletException, IOException {
        int userId = (Integer)session.getAttribute("userId");
        Restaurant restaurant = new Restaurant(request.getParameter("name"),
                                        request.getParameter("description"),
                                        request.getParameter("startingTime"),
                                        request.getParameter("closingTime"));
         Address address = new Address(request.getParameter("addressLineOne"),
                              request.getParameter("addressLineTwo"),
                              request.getParameter("city"),
                              request.getParameter("state"),
                              Integer.parseInt(request.getParameter("pincode")));
        String homeDelivery = request.getParameter("homeDelivery");
        if ((homeDelivery != null) && (!homeDelivery.isEmpty())) {
            restaurant.setHomeDelivery(true);
        } else {
            restaurant.setHomeDelivery(false);
        }
        boolean isSuccesful = restaurantController.registerRestaurant
                                                  (restaurant, address, userId);
        if (isSuccesful) {
            response.sendRedirect("view/UpdateMenuItem.jsp");
        }
    }
    
    /**
     * Adds a new restaurant classification to the database.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    private void addClassification(HttpServletRequest request, 
                                   HttpServletResponse response)
                                   throws ServletException, IOException {
        Classification classification = new Classification();
        classification.setName(request.getParameter("name"));
        restaurantController.addClassification(classification);
    }
    
    /**
     * Removes a classification from the database.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    private void removeClassification(HttpServletRequest request, 
                                   HttpServletResponse response)
                                   throws ServletException, IOException {
        int id =  Integer.parseInt(request.getParameter("id"));
        restaurantController.removeClassification(id);
    }
    
    /**
     * Assign a classification to the restaurant.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void assignClassification(HttpServletRequest request, 
                            HttpServletResponse response, HttpSession session)
                            throws ServletException, IOException {
        Classification classification = 
                        (Classification)request.getAttribute("classification");
        Restaurant restaurant = (Restaurant)request.getAttribute("restaurant");
        restaurantController.assignClassification(classification, restaurant);
    }
    
    /**
     * Displays all the classifications.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void getClassifications(HttpServletRequest request, 
                              HttpServletResponse response, HttpSession session)
                              throws ServletException, IOException {
        Set<Classification> classifications = 
                                      restaurantController.getClassifications();
        request.setAttribute("classifications", classifications);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
    
    /**
     * Displays all the restaurants for the specified classification.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void getRestaurants(HttpServletRequest request, 
                             HttpServletResponse response, HttpSession session)
                             throws ServletException, IOException {
        
        int classificationId = Integer.parseInt(request.getParameter("id"));
        Set<Restaurant> restaurants = 
                              restaurantController.getRestaurants(classificationId);
        request.setAttribute("restaurants", restaurants);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
    
    /**
     * User adds a new review to the restaurant.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    private void addReview(HttpServletRequest request, 
                           HttpServletResponse response, HttpSession session)
                           throws ServletException, IOException {
        User user = (User)session.getAttribute("user");
        Restaurant restaurant = (Restaurant)request.getAttribute("restaurant");
        Review review = new Review(request.getParameter("description"));
        restaurantController.addReview(user, restaurant, review);
    }
}
