package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import com.ideas2it.zomato.user.controller.UserController;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.common.Constant;

/**
 * Servlet implementation for user registration.
 */
public class UserServlet extends HttpServlet {

    private static final String REGISTER = "register";
    private static final String LOGIN = "login";
    private static final String LOGOUT = "logout";
    private static final String RESET_PASSWORD = "Reset Password";
    private static final String GIVE_ADMIN_ACCESS = "give admin access";
    private static final String ADD_ADDRESS = "add address";

    /**
     * Inserts and updates tables into the database for user.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();
        
        switch (input) {
            case REGISTER :
                register(request, response);
                break;
            case LOGIN :
                login(request, response, session);
                break;
            case LOGOUT :
                logout(request, response, session);
                break;
            case RESET_PASSWORD :
                resetPassword(request, response);
                break;
            case GIVE_ADMIN_ACCESS :
                giveAdminAccess(request, response, session);
                break;
            case ADD_ADDRESS :
                addAddress(request, response, session);
                break;
        }
    }
    
    /**
     * Registers a new user to the application.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    private void register(HttpServletRequest request, 
                          HttpServletResponse response)
                          throws ServletException, IOException {
        int roleId = Integer.parseInt(request.getParameter("roleId"));
        User user = new User(request.getParameter("firstName"),
                             request.getParameter("lastName"),
                             Long.parseLong(request.getParameter("mobile")),
                             request.getParameter("email"),
                             request.getParameter("password"),
                             request.getParameter("petName"),
                             request.getParameter("bicycleName"));
        Address address = new Address(request.getParameter("addressLineOne"),
                              request.getParameter("addressLineTwo"),
                              request.getParameter("city"),
                              request.getParameter("state"),
                              Integer.parseInt(request.getParameter("pincode")));
        UserController userController = new UserController();
        boolean isSuccessful = userController.register(user, address, roleId);
        redirectToPage(isSuccessful, Constant.REGISTER_SUCCESFUL, 
                       Constant.ERROR, response);
    }
    
    /**
     * Logs in the user to the application.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void login(HttpServletRequest request, 
                       HttpServletResponse response, HttpSession session)
                       throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        UserController userController = new UserController();
        User user = userController.login(email, password);
        if (user != null) {
            session.setAttribute("user", user);
            session.setAttribute("userId", user.getId());
            redirectToPage(user, response);
        }
    }
    
    /**
     * Logs out the user to the application.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void logout(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        User user = (User)session.getAttribute("user");
        session.invalidate();
        UserController userController = new UserController();
        boolean isSuccessful = userController.logout(user);
        redirectToPage(isSuccessful, Constant.WELCOME, 
                       Constant.INVALID_EMAIL_PASSWORD, response);
    }
    
    /**
     * Enables user to reset old password.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    private void resetPassword(HttpServletRequest request, 
                               HttpServletResponse response)
                               throws ServletException, IOException {
        String email = (request.getParameter("email"));
        String password = request.getParameter("password");
        String petName = (request.getParameter("petName"));
        String bicycleName = (request.getParameter("bicycleName"));
        UserController userController = new UserController();
        boolean isSuccesful = userController.resetPassword(email, password, 
                                                        petName, bicycleName);
        redirectToPage(isSuccesful, Constant.LOGIN_USER, 
                       Constant.INVALID_EMAIL_PASSWORD, response);
    }
    
    /**
     * Adds a new address to the user.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void addAddress(HttpServletRequest request, 
                            HttpServletResponse response, HttpSession session)
                            throws ServletException, IOException {
        Address address = new Address(request.getParameter("addressLineOne"),
                              request.getParameter("addressLineTwo"),
                              request.getParameter("city"),
                              request.getParameter("state"),
                              Integer.parseInt(request.getParameter("pincode")));
        User user = (User)session.getAttribute("user");
        UserController userController = new UserController();
        userController.addAddress(user, address);
        response.sendRedirect(Constant.CUSTOMER_HOME);
    }
    
    /**
     * Redirects to the next jsp page based on the boolean status.
     *
     * @param isSuccessful
     *        - boolean that determines the viewpage.
     *
     * @param successPage
     *        - jsp page is boolean is true.
     *
     * @param failurePage
     *        - jsp page is boolean is false.
     */
    private void redirectToPage(boolean isSuccessful, String successPage,
                             String failurePage, HttpServletResponse response)
                             throws ServletException, IOException {
        if (isSuccessful) {
            response.sendRedirect(successPage);
        } else {
            response.sendRedirect(failurePage);
        }
    }
    
    /**
     * Redirects to the next jsp page based on the user role.
     *
     * @param user
     *        - user that determines the viewpage.
     */
    public void redirectToPage(User user, HttpServletResponse response)
                               throws ServletException, IOException {
        switch (user.getRole().getId()) {
            case 1 :
                response.sendRedirect(Constant.ADMIN_HOME);
                break;
            case 2 :
                response.sendRedirect(Constant.CUSTOMER_HOME);
                break;
            case 3 :
                response.sendRedirect(Constant.RESTAURANT_HOME);
                break;
        }
    }
    
    /**
     * Gives admin access to a normal user.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void giveAdminAccess(HttpServletRequest request, 
                       HttpServletResponse response, HttpSession session)
                       throws ServletException, IOException {
        User adminUser = (User)session.getAttribute("user");
        User user = (User)request.getAttribute("user");
        UserController userController = new UserController();
        userController.giveAdminAccess(adminUser, user);
    }
}
