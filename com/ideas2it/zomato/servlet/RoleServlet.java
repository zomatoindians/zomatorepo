package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.controller.RoleController;

/**
 * Servlet implementation for user roles.
 */
public class RoleServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(RoleServlet.class);

    private static final String ADD_ROLE = "add role";
    private static final String REMOVE_ROLE = "remove role";
    private static final String GET_ROLES = "get roles";

    /**
     * Inserts and updates tables into the database for role .
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
                           throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();
        
        switch (input) {
            case ADD_ROLE :
                add(request, response);
                break;
            case REMOVE_ROLE :
                remove(request, response);
                break;
        }
    }
    
    /**
     * Displays information regarding user roles. 
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
                           throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();
        
        logger.debug("inside doget");
    
        switch (input) {
            case GET_ROLES :
                getRoles(request, response, session);
                break;
        }
    }
    /**
     * Adds a new role to the database.
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     */
    private void add(HttpServletRequest request, HttpServletResponse response)
                     throws ServletException, IOException {
        Role role = new Role(request.getParameter("name"));
        RoleController roleController = new RoleController();
        roleController.add(role);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
    
    /**
     * Removes the role from database.
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     */
    private void remove(HttpServletRequest request, HttpServletResponse response)
                        throws ServletException, IOException {
        int roleId = (Integer)(request.getAttribute("id"));
        RoleController roleController = new RoleController();
        roleController.remove(roleId);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
    
    /**
     * Returns all the roles from the database.
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void getRoles(HttpServletRequest request, 
                          HttpServletResponse response, HttpSession session)
                          throws ServletException, IOException {
        RoleController roleController = new RoleController();
        Set<Role> roles = roleController.getRoles();
        request.setAttribute("roles", roles);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
}
