package com.ideas2it.zomato.tracking;

import java.util.Date;

import com.ideas2it.zomato.order.Order;

/**
 * This is model class for tracking .
 */
public class Tracking {

    private int id;
    private Order order;
    private boolean dispatched;
    private boolean delivered;
    private String arrivalTime;
    
    public Tracking() {
    
    }
    
    public Tracking(Order order) {
        this.order = order;
    }
    
    /*
     * Setters and Getters.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getd() {
        return id;
    }
    
    public void setOrder(Order order) {
        this.order = order;
    }
    
    public Order getOrder() {
        return order;
    }
    
    public void setDispatched(boolean dispatched) {
        this.dispatched = dispatched;
    }
    
    public boolean getDispatched() {
        return dispatched;
    }
    
    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }
    
    public boolean getDelivered() {
        return delivered;
    }
    
    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    
    public String getArrivalTime() {
        return arrivalTime;
    }
}
