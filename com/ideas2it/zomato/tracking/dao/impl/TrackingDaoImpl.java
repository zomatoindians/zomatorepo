package com.ideas2it.zomato.tracking.dao.impl;

import org.hibernate.Query;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.zomato.common.Config;
import com.ideas2it.zomato.tracking.dao.TrackingDao;
import com.ideas2it.zomato.tracking.Tracking;

/*
 * This class handles access between Tracking services and hibernate 
 */
public class TrackingDaoImpl implements TrackingDao {
    
    /**
     * Returns the tracking for the order.
     *
     * @param orderId
     *            - order id for the required tracking .
     *
     * @return tracking of the order.
     */
    public Tracking getTracking(int orderId) {
        Tracking tracking = new Tracking();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Tracking.class);
            criteria.add(Restrictions.eq("orderId", orderId));
            tracking = (Tracking)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return tracking;
    }
}
