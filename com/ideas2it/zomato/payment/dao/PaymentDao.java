package com.ideas2it.zomato.payment.dao;

import java.util.List;

import com.ideas2it.zomato.payment.Payment;

/*
 * This interface handles access between Payment service and hibernate .
 */
public interface PaymentDao {
   
   /**
     * Inserts new payment into the database.
     *
     * @param payment
     *            - payment to be updated.
     */
    void insert(Payment payment);
   
   /**
     * Returns the payment for the specified id.
     *
     * @param id
     *            - id of the payment.
     *
     * @return payment object for the id.
     */
    Payment getPayment(int id);
}
