package com.ideas2it.zomato.payment.service.impl;

import java.util.Set;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cart.service.CartService;
import com.ideas2it.zomato.cart.service.impl.CartServiceImpl;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.service.PaymentService;
import com.ideas2it.zomato.payment.dao.PaymentDao;
import com.ideas2it.zomato.payment.dao.impl.PaymentDaoImpl;
import com.ideas2it.zomato.common.Util;

/*
 * This class handles access between Payment option DAO and controller layer .
 */
public class PaymentServiceImpl implements PaymentService {

    PaymentDao paymentDao = new PaymentDaoImpl();
    CartService cartService = new CartServiceImpl();
    UserService userService = new UserServiceImpl();

    /**
     * Creates a new payment for the cart.
     *
     * @param userId
     *            - user id of the payment.
     *
     * @return payment for the specified user.
     */
    public Payment createPayment(int userId) {
        Cart cart = cartService.getCart(userId);
        User user = userService.getUser(userId);
        Set<CartDetail> cartDetails = cart.getCartDetails();
        float cost = 0.0f;
        for (CartDetail cartDetail : cartDetails) {
            cost += cartDetail.getPrice();
        }
        Payment payment = new Payment();
        payment.setCost(cost);
        payment.setUser(user);
        payment.setTime(Util.getDateTime());
        paymentDao.insert(payment);
        return payment;
    }
    
    /**
     * Returns the payment for the id.
     *
     * @param id
     *            - id of the payment.
     *
     * @return payment for the specified user.
     */
    public Payment getPayment(int id) {
        return paymentDao.getPayment(id);
    }
}
