package com.ideas2it.zomato.category.dao;

import java.util.Set;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * Interface implementation for Category dao class.
 */
public interface CategoryDao {

    /**
     * Inserts a new category into the database.
     *
     * @param category
     *            - category object about to be inserted.
     */
    void insert(Category category);
    
    /**
     * Gets the categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     */
    Set<Category> getCategories(int restaurantId);
    
    /**
     * Get all the categories from the database.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     */
    Set<Category> getAllCategories();
    
    /**
     * Deletes the category from the database specified by id.
     *
     * @param id
     *            - id of category to be deleted.
     */
    void delete(int id);
}
