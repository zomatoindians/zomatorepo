package com.ideas2it.zomato.category;

import java.util.Set;

import com.ideas2it.zomato.menuItem.MenuItem;

/**
 * Category of available foods. 
 */
public class Category {

    private int id;
    private String name;
    private Set<MenuItem> menuItems;
    
    public Category() {
    
    }
    
    public Category(String name) {
        this.name = name;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setMenuItems(Set<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
    
    public Set<MenuItem> getMenuItems() {
        return menuItems;
    }
}
