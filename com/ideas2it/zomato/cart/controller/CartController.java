package com.ideas2it.zomato.cart.controller;

import java.lang.NullPointerException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cart.service.CartService;
import com.ideas2it.zomato.cart.service.impl.CartServiceImpl;

/**
 * Controller implementation for user cart.
 */
public class CartController {

    CartService cartService = new CartServiceImpl();
    private static final Logger logger = Logger.getLogger(CartController.class); 
    
    /**
     * User adds a new item to the cart.
     *
     * @param menuItemId
     *            - id of the chosen menu item.
     *
     * @param userId
     *            - id of cart's user.
     */
    public void addToCart(int menuItemId, int userId) {
        try {
            cartService.addToCart(menuItemId, userId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Returns the cart for the specified user.
     *
     * @param userId
     *            - id of cart's user.
     *
     * @return Cart for the specified user.
     */
    public Cart getCart(int userId) {
        Cart cart = new Cart();
        try {
            cart = cartService.getCart(userId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return cart;
    }
    
    /**
     * Removes the cart detail specified, from the cart.
     *
     * @param cartDetailId
     *            - id of cartDetail.
     *
     * @return True or false based on cart detail deletion.
     */
    public boolean removeCartDetail(int cartDetailId) {
        boolean value = false;
        try {
            value = cartService.removeCartDetail(cartDetailId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return value;
    }
}
