package com.ideas2it.zomato.cart.dao.impl;

import org.hibernate.Query;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.zomato.common.Config;
import com.ideas2it.zomato.cart.dao.CartDao;
import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;

/*
 * This class handles access between Cart Service and hibernate .
 */
public class CartDaoImpl implements CartDao {

    /**
     * Returns the cart for the specified user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return cart object for the id.
     */
    public Cart getCart(int userId) throws HibernateException {
        Cart cart = new Cart();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Cart.class);
            criteria.add(Restrictions.eq("userId", userId));
            cart = (Cart)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return cart;
    }
    
    /**
     * Updates the cart into the database.
     *
     * @param cart
     *            - cart to be updated.
     */
    public void update(Cart cart) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.saveOrUpdate(cart); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Deletes the cart detail from the database specified by id.
     *
     * @param id
     *            - id of cart detail to be deleted.
     *
     * @return true or false based on cart detail removal.
     */
    public boolean removeCartDetail(int id) throws HibernateException {
        boolean isSuccesful = false;
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            CartDetail cartDetail = (CartDetail) session.get
                                                 (CartDetail.class, id);
            session.delete(cartDetail);
            transaction.commit();
            isSuccesful = true;
        } catch (HibernateException exception) {
            if (null != transaction) { 
                transaction.rollback();
            } 
            isSuccesful = false;
            throw exception;
        } finally {
            session.close(); 
        }
        return isSuccesful;
    }
}
