package com.ideas2it.zomato.classification.service;

import java.util.Set;

import com.ideas2it.zomato.classification.Classification;

/**
 * Interface implementation for Classification service class.
 */
public interface ClassificationService {

    /**
     * Adds a new classification.
     *
     * @param classification
     *            - classification object about to be added .
     */
    void add(Classification classification);
    
    /**
     * Returns all the classifications in the database.
     *
     * @return set of classifications.
     */
    Set<Classification> getClassifications();
    
    /**
     * Removes the classification .
     *
     * @param id
     *            - id of classification .
     */
    void remove(int id);
}
