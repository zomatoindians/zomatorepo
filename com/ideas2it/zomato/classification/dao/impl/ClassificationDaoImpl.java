package com.ideas2it.zomato.classification.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.classification.dao.ClassificationDao;
import com.ideas2it.zomato.common.Config;


/**
 * Performs all the database related funcionalities for Classification object.
 */
public class ClassificationDaoImpl implements ClassificationDao {

    /**
     * Inserts a new classification into the database.
     *
     * @param classification
     *            - classification object about to be inserted.
     */
    public void insert(Classification classification) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.save(classification); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Returns all the classifications from the database.
     *
     * @return set of classifications.
     */
    public Set<Classification> getClassifications() throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        Set<Classification> classifications = new HashSet<>();
        try {
           transaction = session.beginTransaction();
           classifications = new HashSet(session
                                 .createQuery("FROM Classification").list());
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
        return classifications;
    }
        
    /**
     * Deletes the classification from the database specified by id.
     *
     * @param id
     *            - id of classification to be deleted.
     */
    public void deleteClassification(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           Classification classification = (Classification)session
                                            .get(Classification.class, id);
           session.delete(classification);
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
}
