package com.ideas2it.zomato.review;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * Review left by a user for either a menuitem or a restaurant. 
 */
public class Review {

    private int id;
    private String description;
    private User user;
    private MenuItem menuItem;
    private Restaurant restaurant;
    
    public Review() {
    
    }
    
    public Review(String description) {
        this.description = description;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
    
    public MenuItem getMenuItem() {
        return menuItem;
    }
    
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
    
    public Restaurant getRestaurant() {
        return restaurant;
    }

}
