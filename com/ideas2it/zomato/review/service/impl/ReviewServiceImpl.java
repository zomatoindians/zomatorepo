package com.ideas2it.zomato.review.service.impl;

import com.ideas2it.zomato.review.Review;
import com.ideas2it.zomato.review.service.ReviewService;
import com.ideas2it.zomato.review.dao.ReviewDao;
import com.ideas2it.zomato.review.dao.impl.ReviewDaoImpl;

/**
 * Service class implementation for review .
 */
public class ReviewServiceImpl implements ReviewService {

    ReviewDao reviewDao = new ReviewDaoImpl();
    
    /**
     * Adds a new review to the database.
     *
     * @param review
     *            - review to be added.
     */
    public void add(Review review) {
        reviewDao.insert(review);
    }
    
    /**
     * Deletes a review from the database.
     *
     * @param review
     *            - review to be removed.
     */
    public void remove(Review review) {
        reviewDao.delete(review.getId());
    }
}
