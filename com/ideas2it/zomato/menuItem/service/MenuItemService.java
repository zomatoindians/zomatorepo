package com.ideas2it.zomato.menuItem.service;

import java.util.Set;

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * Interface implementation for MenuItem service class.
 */
public interface MenuItemService {

    /**
     * Registers a new menu item to the restaurant.
     *
     * @param menuItem
     *        - menuItem to be added.
     *
     * @param restaurantId
     *        - restaurant id of the menuitem.
     */
    void addMenuItem(MenuItem menuItem, int restaurantId);
    
    /**
     * Updates a new menuItem.
     *
     * @param menuItem
     *            - menuItem object about to be updated .
     */
    void update(MenuItem menuItem);
    
    /**
     * Deletes a menuItem.
     *
     * @param id
     *            - id of menuItem to be deleted.
     */
    void delete(int id);
    
    /**
     * Get the menu items for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required .
     *
     * @param categoryId
     *            - category Id whose menu items are required .
     *
     * @return allmenuitems belonging to the restaurant.
     */
    Set<MenuItem> getMenuItems(int restaurantId, int categoryId);
    
    /**
     * Returns menu item specified by id.
     *
     * @param id
     *            - id of menuItem to be returned.
     */
    MenuItem getMenuItem(int id);
}
