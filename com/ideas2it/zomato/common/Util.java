package com.ideas2it.zomato.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Date;
import java.text.SimpleDateFormat;

public class Util {
    
    /**
     * Validates the email id entered by user.
     *
     * @param email
     *            - email id about to be validated .
     *
     * @return true or false based on email correctness.
     */
    public static boolean validateEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    /**
     * Validates the mobile number entered by user.
     *
     * @param number
     *            - mobile number to be validated .
     *
     * @return true or false based on mobile number correctness.
     */
    public static boolean validateMobileNumber(long number) {
        boolean isValid = false;
        long length = (long)(Math.log10(number)+1);
        if (length == 10) {
            isValid = true;
        }
        return isValid;
    }
    
    /**
     * Sets the current date and time.
     *
     * @return current date time.
     */
    public static String getDateTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = 
                                    new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return (simpleDateFormat.format(date));
    }
}
