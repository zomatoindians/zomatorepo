package com.ideas2it.zomato.user.controller;

import java.lang.NullPointerException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;

/**
 * Controller implementation for user servlet.
 */
public class UserController {

    UserService userService = new UserServiceImpl();
    private static final Logger logger = Logger.getLogger(UserController.class);
    
    /**
     * Registers a new user.
     *
     * @param user
     *            - user object about to be added .
     *
     * @param address
     *            - address of the user to be added.
     *
     * @return true or false based on user group insertion.
     */
    public boolean register(User user, Address address, int roleId) {
        boolean value = false;
        try {
            value = userService.register(user, address, roleId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return value;
    }
    
    /**
     * Adds a new address to the user.
     *
     * @param user
     *            - user object about to be added .
     *
     * @param address
     *            - address of the user to be added.
     *
     * @return true or false based on address assigning.
     */
    public boolean addAddress(User user, Address address) {
        boolean value = false;
        try {
            value = userService.addAddress(user, address);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return value;
    }
    
    /**
     * Logs in the user to the application.
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @return user that is logged in.
     */
    public User login(String email, String password) {
        User user = new User();
        try {
            user = userService.login(email, password);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return user;
    }
    
    /**
     * Logs out the user from the application.
     *
     * @param user
     *            - user to be logged out.
     *
     * @return true or false based on user group insertion.
     */
    public boolean logout(User user) {
        boolean value = false;
        try {
            value = userService.logout(user.getId());
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return value;    
    }
    
    /**
     * Enables user to reset old password.
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @param petName
     *            - pet name of the user.
     *
     * @param bicycleName
     *            - bicycle name of the user .
     *
     * @return true or false based on user group insertion.
     */
    public boolean resetPassword(String email, String password, String petName, 
                              String bicycleName) {
        boolean value = false;
        try {
            value = userService.resetPassword(email, password, petName, bicycleName);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return value;
    }
    
    /**
     * Converts normal user to a admin user.
     *
     * @param adminUser
     *            - adminUser who grants admin access .
     *
     * @param user
     *            - user who is assigned as admin.
     *
     * @return true or false based on giving admin access.
     */
    public boolean giveAdminAccess(User adminUser, User user) {
        boolean value = false;
        try {
            value = userService.giveAdminAccess(adminUser, user);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return value;
    }
}
