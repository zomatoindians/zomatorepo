package com.ideas2it.zomato.user.dao.impl;

import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.common.Config;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.dao.UserDao;

/**
 * Performs insert, update, delete, and read funcionalities for user.
 */
public class UserDaoImpl implements UserDao {

    /**
     * Inserts a new user into the database.
     *
     * @param user
     *            - user object about to be inserted.
     *
     * @return true or false based on user group deletion.
     */
    public boolean insert(User user) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        boolean success = false;
        try {
           transaction = session.beginTransaction();
           session.save(user);
           transaction.commit();
           success = true;
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
                success = false;
            } 
            throw exception;
        } finally {
             session.close();
        }
        return success;
    }
    
    /**
     * Updates the user in the database.
     *
     * @param user
     *            - user object to be updated.
     */
    public void update(User user) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.update(user); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Gets a user from database based on email.
     *
     * @param email
     *            - email of the user.
     *
     * @return user specified by email.
     */
    public User getUserByEmail(String email) throws HibernateException {
        User user = new User();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("email", email));
            user = (User)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return user;
    }
    
    /**
     * Gets a user from database based on id.
     *
     * @param id
     *            - id of the user.
     *
     * @return user specified by id.
     */
    public User getUserById(int id) throws HibernateException {
        User user = new User();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("id", id));
            user = (User)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return user;
    }
    
    /**
     * Deletes the user from the database specified by id.
     *
     * @param id
     *            - id of user to be deleted.
     */
    public void delete(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            User user = (User) session.get(User.class, id);
            session.delete(user);
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) { 
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
    }
    
    /**
     * Returns the user if he has admin rights.
     *
     * @param id
     *            - id of the user.
     *
     * @return admin user specified by id.
     */
    public User returnIfAdmin(int id) throws HibernateException {
        User user = new User();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            user = (User)session.createCriteria(User.class)
                            .add( Restrictions.eq("id", id) )
                            .createCriteria("role")
                            .add( Restrictions.eq("name", "admin") )
                            .uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw exception;
        } finally {
            session.close(); 
        }
        return user;
    }
}
